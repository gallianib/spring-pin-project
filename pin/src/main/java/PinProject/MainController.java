package PinProject;

import PinProject.PinTable.Pins;
import PinProject.PinTable.PinsRepository;
import PinProject.UserTable.User;
import PinProject.UserTable.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.SessionCookieConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;


@Controller
    @RequestMapping(path="/pins")
    public class MainController {

        @Autowired
        private UserRepository userRepository;

        @Autowired
        private PinsRepository pinsRepository;

    private void WriteToFile(String str)throws IOException {
        FileWriter fileWriter;
        if (userRepository.findAll() == null){
            fileWriter = new FileWriter("src/main/java/PinProject/output.txt");
        }else{ fileWriter = new FileWriter("src/main/java/PinProject/output.txt",true);}
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println(str);
        printWriter.close();
    }
    private String convMStoDATE(Long ms){
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        Date resultdate = new Date(ms);
        return (sdf.format(resultdate));
    }
    @RequestMapping(path="/viewLog",produces ="application/json")
    public @ResponseBody String viewLog(){
        try {
            return new Scanner(new File("src/main/java/PinProject/output.txt"))
                    .useDelimiter("\\A").next();

        }catch (Exception e){}
        return "";
    }

    @RequestMapping(path="/add",method = RequestMethod.POST,produces ="application/json")
    public
    String addNewUser (@RequestParam(value ="account")String account,@RequestParam(value = "password")String password) {

        User user = new User();
        user.setAccount(account);
        user.setPassword(password);
        user.setPin(null);
        user.setClaim_ip(null);
        user.setClaim_timestamp(null);
        user.setClaim_user(null);
        user.setExpire_timestamp(null);
        user.setCreate_user(null);
        userRepository.save(user);
        try {
            WriteToFile("CREATED NEW USER: " + user.getAccount() + " / CURRENT TIME: "+convMStoDATE(System.currentTimeMillis()));
        }catch (Exception e){}
        return "redirect:/";
    }


    @RequestMapping(path="/user/",method = RequestMethod.POST,produces ="application/json")
    public @ResponseBody
    String getUserByID (@CookieValue(value = "id") Long id) {

        User user=userRepository.findOne(id);

        if(user==null)
            return "result: user not found";
        return "Id: "+user.getId()+"\n" +
                "Account: "+user.getAccount()+"\n"+
                "Pin: "+user.getPin()+"\n"+
                "Pin ID: "+user.getPid()+"\n"+
                "Create User: "+user.getCreate_user()+"\n"+
                "Create Time: "+user.getCreate_timestamp()+"\n"+
                "Create IP: "+user.getCreate_ip()+"\n"+
                "Expire Time: "+user.getExpire_timestamp()+"\n"+
                "Last Claim User: "+user.getClaim_user()+"\n"+
                "Last Claim Time: "+user.getClaim_timestamp()+"\n"+
                "Last Claim IP: "+user.getClaim_ip()+"\n";
    }


    @RequestMapping(path="/user/delete/",method = RequestMethod.POST,produces ="application/json")
    public @ResponseBody
    String deleteUserByID (@RequestParam(value = "id") Long id) {

        User user=userRepository.findOne(id);
        if(user==null)
            return "{\"result\":\"user not found\"}";
        userRepository.delete(user);
        try {
            WriteToFile("DELETED USER: " + user.getAccount() + " / CURRENT TIME: " +convMStoDATE(System.currentTimeMillis()));
        }catch (Exception e){}
        return "redirect:/";
    }

    @RequestMapping(path="/user/all",method = RequestMethod.GET,produces ="application/json")
    public @ResponseBody Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @RequestMapping(path="/login",method = RequestMethod.POST,produces ="application/json")
    public String requestLogin (@RequestParam("account") String account, @RequestParam("password") String password, HttpServletRequest request, HttpServletResponse response) {
        Iterable<User> all = userRepository.findAll();
        for (User user : all) {
            if (user.getAccount().equals(account) && user.getPassword().equals(password)){
                response.addCookie(new Cookie("id",user.getId().toString()));
                backgroundCheck(user.getId());
                return "redirect:/home.html";
            }
        }
        return "redirect:/";

    }
        private void backgroundCheck(Long uid){
            User user = userRepository.findOne(uid);
            Calendar calendar = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
            if(user.getPid() != null && user.getPid() > 0) {
                if (timestamp.after(user.getExpire_timestamp())) {
                    pinsRepository.delete(user.getPid());
                    user.setPin("expire");
                    user.setCreate_timestamp(null);
                    user.setPid(null);
                    user.setExpire_timestamp(null);
                    user.setCreate_user(null);
                    user.setCreate_ip(null);
                    userRepository.save(user);
                }
            }else if(user.getPid() == null){
                user.setPin("null");
                user.setCreate_timestamp(null);
                user.setExpire_timestamp(null);
                user.setCreate_user(null);
                user.setCreate_ip(null);
                userRepository.save(user);
            }
        }

        private boolean checkPin(String pin){
            boolean same = false;
            Iterable<Pins> pins = pinsRepository.findAll();
            for (Pins tmpPin : pins) {
                if (pin.equals(tmpPin.getPin())){
                    same = true;
                }
            }

            return same;
        }
    public static String mod10Check(String pin)
    {
        int sum = 0;
        boolean alternate = true;
        for (int i = pin.length() - 1; i >= 0; i--)
        {
            int n = Integer.parseInt(pin.substring(i, i + 1));
            if (alternate)
            {
                n *= 2;
                if (n > 9)
                {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        if ((sum % 10) == 0 )
        {
            return "0";
        }
        return Integer.toString(10 - (sum % 10));
    }

        private String getPin(){
            Random r = new Random();
            String pin = "";
            int tmp = 0;
            for (int i = 0;i<5;i++){
                if(i == 0){
                    tmp = r.nextInt(8)+1;
                    pin += String.valueOf(tmp);
                }else {
                    tmp = r.nextInt(9);
                    pin += String.valueOf(tmp);
                }
            }
            if(checkPin(pin)){
                getPin();
            }
            pin += mod10Check(pin);

            return pin;
        }
    @RequestMapping(path="/getPin",method = RequestMethod.POST,produces ="application/json")
    public String getNewUserPin(HttpServletRequest request,@CookieValue("id")Long id) {

        User user = userRepository.findOne(id);
        if (user == null)
            return "{\"result\":\"user not found\"}";
        String ip = getIpAddr(request);
        user.setCreate_ip(ip);
        String pin = getPin();
        if (user.getPid() != null && user.getPid() > 0)pinsRepository.delete(user.getPid());
        user.setPin(pin);
        user.setCreate_user(user.getAccount());
        Pins newPin = new Pins();
        Timestamp createTS = new Timestamp(System.currentTimeMillis());
        user.setCreate_timestamp(createTS);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(createTS);
        calendar.add(Calendar.MINUTE, 30);
        Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
        user.setExpire_timestamp(timestamp);
        newPin.setPin(pin);
        newPin.setUid(id);
        pinsRepository.save(newPin);
        user.setPid(newPin.getPid());
        userRepository.save(user);
        try {
            WriteToFile("USER: " + user.getAccount() + " RECEIVED PIN: " + pin + " / CURRENT TIME: "+convMStoDATE(System.currentTimeMillis()));
        }catch (Exception e){}
        return "redirect:/home.html";
    }

    @RequestMapping(path="/claimPin",method = RequestMethod.POST,produces ="application/json")
    public @ResponseBody String claimUserPin(HttpServletRequest request,@RequestParam("pin")String pin,@CookieValue("id")Long id) {
        Iterable<Pins> p = pinsRepository.findAll();
        User claimAcc = userRepository.findOne(id);
        for (Pins ps : p) {
            if (pin.equals(ps.getPin())){
                Long uid = ps.getUid();
                User pinHolder = userRepository.findOne(uid);
                Calendar calendar = Calendar.getInstance();
                Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
                if (timestamp.before(pinHolder.getExpire_timestamp())){
                    pinHolder.setClaim_ip(getIpAddr(request));
                    pinHolder.setClaim_timestamp(new Timestamp(System.currentTimeMillis()));
                    pinHolder.setClaim_user(claimAcc.getAccount());
                    pinHolder.setPin("used");
                    pinHolder.setPid(null);
                    pinsRepository.delete(ps.getPid());
                    userRepository.save(pinHolder);
                    try {
                        WriteToFile("USER: "+claimAcc.getAccount()+" AT IP: " + pinHolder.getClaim_ip() + " CLAIMED PIN: " + pin + " FROM USER: "+pinHolder.getAccount() +" / CURRENT TIME: "+convMStoDATE(System.currentTimeMillis()));
                    }catch (Exception e){}
                    return "Claimed!";
                }else {
                    backgroundCheck(pinHolder.getId());
                    return "Pin expired!";
                }
            }
        }

        return "wrong pin?";
    }

    private static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_X_FORWARDED");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_FORWARDED");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("HTTP_VIA");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getHeader("REMOTE_ADDR");
        }
        if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    }
